"""
Crear una función para sumar los valores recibidos de tipo numérico,
utilizando argumentos variables*args como parámetro de la función
y regresar como resultado la suma de todos los valores pasados como argumentos.
"""
def sumarXNumeros(*args):
    resultado = 0
    for x in args:
        resultado += int(x)
    return resultado

def SumarXNumeros(args):
    resultado = 0
    for x in args:
        resultado += int(x)
    return resultado

print('************* se sumara 4 + 15 + 20 +5 ***************')
print(f'El resultado de la suma es {sumarXNumeros(4, 15, 20, 5)}')


cantidad = int(input('Cuantos numeros quieres sumar: '))
listaNumeros = []
for i in range(0, cantidad):
    listaNumeros += (input('Digite un numero para sumar: '),)

print('****************************')
print(f'El resultado de la suma es {SumarXNumeros(listaNumeros)}')
