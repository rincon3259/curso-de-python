"""
Ejercicio:Convertidor de Temperatura
Realizar dos funciones para convertir de grados celsius a fahrenheit y viceversa.
"""
def convertirCelsius(args):
    celsius = (args - 32) * (5/9)
    return celsius

def convertirFahrenheit(args):
    fahrenheit = (args * (9/5)) + 32
    return fahrenheit

opcion = input('Desea convertir Fahrenheit a celsius digita 1: \n'
            'Desea convertir celsius a fahrenheit digita 2: \n')
actual = ''
cambio = ''
if opcion == '1':
    temperatura = float(input('Digite la temperatura en grados fahrenheit para convertir a celsius: \n'))
    nuevaTemperatura = convertirCelsius(temperatura)
    actual = 'celsius'
    cambio = 'fahrenheit'
elif opcion == '2':
    temperatura = float(input('Digite la temperatura en grados celsius para convertir a fahrenheit: \n'))
    nuevaTemperatura = convertirFahrenheit(temperatura)
    actual = 'celsius'
    cambio = 'fahrenheit'
else:
    temperatura = 'ERROR - '
    actual = 'Opción '
    nuevaTemperatura = 'no '
    cambio = 'identificada'

print(f'{temperatura}° {actual}  =  {nuevaTemperatura}° {cambio}')
