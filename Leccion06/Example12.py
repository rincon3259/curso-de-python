# -----------  clases y objetos. ----------
# Creando una clase vacía
"""
class Perro:
    pass
"""
# Creamos un objeto de la clase perro
"""
mi_perro = Perro()
"""
class Perro:
    # Atributo de clase
    especie = 'mamífero'

    # El método __init__ es llamado al crear el objeto
    def __init__(self, nombre, raza):
        print(f"Creando perro {nombre}, {raza}")

        # Atributos de instancia
        self.nombre = nombre
        self.raza = raza

    def ladra(self):
        print("Guau")

    def camina(self, pasos):
        print(f"Caminando {pasos} pasos")

print("****************************************************************")

mi_perro = Perro("Toby", "Bulldog")
mi_perro.ladra()
mi_perro.camina(10)

# Creando perro Toby, Bulldog
# Guau
# Caminando 10 pasos

mi_perro.raza = 'Schnauzer'
mi_perro.nombre = 'mako'
print(f'Mi perro se llama {mi_perro.nombre} y es de raza {mi_perro.raza}')
# Mi perro se llama mako y es de raza Schnauzer
mi_perro.color = 'blanco y gris' #Agregamos un nuevo atributo el color del perro
