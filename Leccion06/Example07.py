def desplegarNombres(nombres):
    print(f'{nombres} es de tipo {type(nombres)}')
    for nombre in nombres:
        print(nombre)
    return

nombres = ['Juan', 'Karla', 'Guillermo']
desplegarNombres(nombres) #Lista
desplegarNombres('Carlos') # string
desplegarNombres((10 , 11)) #Tupla
desplegarNombres([9 , 20]) #Lista