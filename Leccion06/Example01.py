# funciones de python
def mi_funcion():
    print('Saludos desde mi función')

mi_funcion()
mi_funcion()
mi_funcion()
print('************************************************')
def f(x):
    return 2*x
y = f(3)
print(y) # 6
print('************************************************')
def di_hola():
    print("Hola")
di_hola() # Hola
print('************************************************')
def di_hola(nombre):
    print("Hola", nombre)
di_hola("Juan")
# Hola Juan
print('************************************************')
def resta(a, b):
    return a-b
print(resta(5, 3)) # 2
#print(resta(3))  # Error! TypeError
#resta(5,4,3) # Error
#TypeError: resta() takes 2 positional arguments but 3 were given
print(resta(a=3, b=5)) # -2
print(resta(b=5, a=3)) # -2
#print(resta(b=5, c=3)) # Error!
#resta() got an unexpected keyword argument 'c'
print('************************************************')
def suma(a, b, c=0):
    return a+b+c
print(suma(5,5,3)) # 13
print(suma(4,3)) # 7
print('************************************************')
def suma(a=3, b=5, c=0):
    return a+b+c
print(suma()) # 8
print(suma(1)) # 6
print(suma(4, 5)) # 9
print(suma(5, 3, 2)) # 10
print(suma(a=5, b=3)) #8
print('************************************************')
def suma(numeros):
    total = 0
    for n in numeros:
        total += n
    return total
print(suma([1,3,5,4])) # 13
print('************************************************')
def suma(*numeros):
    print(type(numeros))
    # <class 'tuple'>
    total = 0
    for n in numeros:
        total += n
    return total
print(suma(1, 3, 5, 4)) # 13
print(suma(6)) # 6
print(suma(6, 4, 10)) # 20
print(suma(6, 4, 10, 20, 4, 6, 7)) # 57
print('************************************************')
def suma(**kwargs):
    suma = 0
    print(type(kwargs))
    for key, value in kwargs.items():
        print(key, value)
        suma += value
    return suma
print(suma(a=5, b=20, c=23)) # 48
print('************************************************')
def suma(**kwargs):
    print(type(kwargs))
    suma = 0
    for key, value in kwargs.items():
        print(key, value)
        suma += value
    return suma
di = {'a': 10, 'b':20}
print(suma(**di)) # 30
print('************************************************')
def mi_funcion():
    print("Entra en mi_funcion")
    return
    print("No llega")
mi_funcion() # Entra en mi_funcion
print('************************************************')
def di_hola():
    return "Hola"
print(di_hola())
# 'Hola'
print('************************************************')
def suma_y_media(a, b, c):
    suma = a+b+c
    media = suma/3
    return suma, media
suma, media = suma_y_media(9, 6, 3)
print(suma)  # 18
print(media) # 6.0
print('************************************************')
def mi_funcion_suma(a, b):
    """
    Descripción de la función. Como debe ser usada,
    que parámetros acepta y que devuelve
    """
    return a+b
help(mi_funcion_suma)
# Help on function mi_funcion_suma in module __main__:

# mi_funcion_suma(a, b)
#     Descripción de la función. Como debe ser usada,
#     que parámetros acepta y que devuelve
print('************************************************')
print(mi_funcion_suma.__doc__)
    # Descripción de la función. Como debe ser usada,
    # que parámetros acepta y que devuelve
print('************************************************')
def multiplica_por_3(numero: int) -> int:
    return numero*3
print(multiplica_por_3(6)) # 18
print(multiplica_por_3("Cadena"))
# 'CadenaCadenaCadena'
print('************************************************')
def suma(a, b):
    return a + b

resultado = suma(5, 3)
print(f'El resultado de la suma es {resultado}')