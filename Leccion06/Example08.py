"""
Funciones recursivas es una función que se manda llamar a sí misma para completar una cierta tarea
el ejemplo clasico es encontrar el factorial de x numero. en este caso el numero 5
# 5! = 5 * 4 * 3 * 2 * 1
# 5! = 5 * 4 * 3 * 2
# 5! = 5 * 4 * 6
# 5! = 5 * 24
# 5! = 120
"""
def factorial(args):
    if args == 1:
        return 1
    else:
        return args * factorial(args-1)

resultado = factorial(5)
print(f'El factorial de 5! es = {resultado} ')

resultado = int(input('Engresa un numero del que quieras conocer su factorial: '))
print(f'El factorial de {resultado}! es = {factorial(resultado)} ')