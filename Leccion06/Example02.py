# def suma(a:int = 0, b:int = 0) -> int:  Esta es una manera de dar una pista para saber que tipo de dato resive 
# y que tipo de datos devuelve.
def suma(a = 0, b = 0):
    return a + b

resultado = suma(5, 3)
print(f'El resultado de la suma es {resultado}')
print(f'El resultado de la suma es {suma()}')
print(f'El resultado de la suma es {suma(5, 3)}')