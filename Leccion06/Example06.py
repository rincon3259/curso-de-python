def listarTerminos(**kwargs):
    for llave, valor in kwargs.items():
        print(f'{llave} = {valor}')


listarTerminos(IDE='Integrated Development Environment',
    OOP='Object Oriented Programming')
listarTerminos(DBMS='Database Management System',
    PK = 'Primary key')