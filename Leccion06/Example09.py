# Ejecicio practico, haciendo uso de funciones recursivas.
# Imprimir una lista de numeros de manera decendente desde.
# N luego N-1, N-2, N-3 hasta llegar a 1. y si se ingresa.
# un numero degativo no imprime nada.
def listaNumeros(args):
    if args <= 0:
        return 'ERROR has ingresado un numero degativo o 0'
    elif args == 1:
        return 1
    else:
        print(args)
        return listaNumeros(args-1)

numero = int(input('Ingrese un numero: '))
print(listaNumeros(numero))