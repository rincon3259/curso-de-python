"""
Crear una función para multiplicar los valores recibidos de tipo numérico,
utilizando argumentos variables*args como parámetro de la función
yregresar como resultado la multiplicación de todos los valores pasados como argumentos.
"""
def multiplicarXNumeros(*args):
    resultado = 1
    for x in args:
        resultado *= x
    return resultado

def MultiplicarXNumeros(args):
    resultado = 1
    for x in args:
        resultado *= int(x)
    return resultado

print('************* se Multiplicara 3 * 15 * 5 * 3 ***************')
print(f'El resultado de la multiplicación es {multiplicarXNumeros(3, 15, 5, 3)}')

cantidad = int(input('Cuantos numeros quieres Multiplicar: '))
listaNumeros = []
for i in range(0, cantidad):
    listaNumeros += (input('Digite un numero para Multiplicar: '),)

print('****************************')
print(f'El resultado de la Multiplicación es {MultiplicarXNumeros(listaNumeros)}')