from FiguraGeometrica import FiguraGeometrica #from modulo 'nombre archivo' impor Clase a utilizas
from Color import Color


class Cuadrado(FiguraGeometrica, Color):#Clase hija llama las clases FiguraGeometrica y Color
    """
        Para inicializar los objetos de la clase padre utilizamos el método super()
        el cual nos permite acceder a los atributos y métodos de la clase padre desde
        una de sus hijas. Sin embargo en herencia múltiple tenemos un problema ya que
        la clase padre es  más de 1 objeto. En este ejemplo tenemos como clase padre
        a FiguraGeometrica y Color. ¿Entonces qué objeto utilizara super() como
        clase padre, la respuesta correcta es que utilizara la primera clase que
        encuentra, para este ejemplo 'class Cuadrado(FiguraGeometrica, Color):'
        FiguraGeometrica es la primera clase y la que utilizara el método super().....
        Que para con la otra o otras clases para inicializarlas, en estos casos cuando
        trabajamos herencia múltiple en python, esta sintaxis no es muy recomendable
        ya que podrían quedar dudas de cual es la clase que se manda llamar, una de
        las soluciones  complementa la función super() con el método __init__()
        Para utilizar las clases padres según sus atributos. FiguraGeometrica tiene 2
        atributos y Color tiene 1 atributo, así que si utilizamos __init__() con 2
        atributos se llama la clase FiguraGeometrica, pero si utilizamos 1 atributo
        llama la clase Color.
        """
    def __init__ (self , lado, color):
        #super().__init__(lado, lado)
        """
        Como es un cuadrado su alto y ancho miden lo mismo, al utilizar 2 atributos el
        método super() __init__() está llamando a la clase FiguraGeometrica como la clase
        padre, este método funciona pero sigue generando algo de confusión.
        Para solucionar esta ambigüedad en herencia múltiple vamos a utilizar el nombre de
        la clase a utilizar, complementamos con el método __init__() y el primer parámetro
        de entrada debe ser la referencia sí mismo ‘selft’ luego si pasamos los atributos de la clase.
        """
        FiguraGeometrica.__init__(self, lado, lado)#lado por lado se traduce en la clase padre como alto y ancho.
        Color.__init__(self, color)

    def calcular_area (self):
        return self.alto * self.ancho  #Esta tomando los atributos heredados de la clase padre FiguraGeometrica

    def __str__(self):
        """ el metodo str nos permite devolver un texto o valores cuando
        se llama el objeto, en ves de devolver el espacio en memoria
        es igual a devolver el estado actual de este objeto."""
        return f'Cuadrado: [ Area: {self.calcular_area()}, Color: {self.color} {super().__str__()}]' #dato, valor o datos a retornar cuando llaman al objeto.