class Color:
    def __init__ (self, color):
        self.__color = color

    @property #decorador para utilizar el metodo como atributo o variable fuera de la clase
    def color (self) : #metodo get para recuperar el atributo color fuera de la clase.
        return {self.__color}

    @color.setter #decorador para editar como atributo o variable fuera de la clase al atributo
    def color (self, color) :#metodo set para editar el atributo color fuera de la clase.
        self.__color = color

    def __str__(self):
        return f'Color: {self.color}'
