from FiguraGeometrica import FiguraGeometrica #from modulo 'nombre archivo' impor Clase a utilizas
from Color import Color


class Rectangulo(FiguraGeometrica, Color):#Clase hija llama las clases FiguraGeometrica y Color
    def __init__ (self , base, altura, color):
        FiguraGeometrica.__init__(self, altura, base)#base por altura se traduce en la clase padre como alto y ancho.
        Color.__init__(self, color)

    def calcular_area (self):
        return self.alto * self.ancho  #Esta tomando los atributos heredados de la clase padre FiguraGeometrica

    def __str__(self):
        #return f'Rectangulo: [ Area: {self.calcular_area()}, Color: {self.color} {super().__str__()}]' #dato, valor o datos a retornar cuando llaman al objeto.
        return f'{FiguraGeometrica.__str__(self)}, {Color.__str__(self)}, Rectangulo: area[{Rectangulo.calcular_area(self)}]'