class FiguraGeometrica:
    def __init__ (self, ancho, alto): # Validamos que alto y ancho sea mayor a 0 y menor a 0
        if self.__validar_valor(ancho):
            self.__ancho = ancho
        else:
            self.__ancho = 0
            print(f'Valor erroneo ancho: {ancho}')
        if self.__validar_valor(alto): # Validamos mediante un metodo
            self.__alto = alto
        else:
            self.__alto = 0
            print(f'Valor erroneo alto: {alto}')

    @property #decorador para utilizar el metodo como atributo o variable fuera de la clase
    def ancho (self) : #metodo get para recuperar el atributo ancho fuera de la clase.
        #print('Llamando método get ancho')
        return self.__ancho

    @ancho.setter #decorador para editar como atributo o variable fuera de la clase al atributo
    def ancho (self, ancho) :#metodo set para editar el atributo ancho fuera de la clase.
        #print('Llamando método set ancho')
        if self.__validar_valor(ancho):
            self.__ancho = ancho
        else:
            print(f'Valor erroneo ancho: {ancho}')

    @property #decorador para utilizar el metodo como atributo o variable fuera de la clase
    def alto (self) : #metodo get para recuperar el atributo alto fuera de la clase.
        return self.__alto

    @alto.setter #decorador para editar como atributo o variable fuera de la clase al atributo
    def alto (self, alto) :#metodo set para editar el atributo alto fuera de la clase.
        if self.__validar_valor(alto): # Validamos mediante un metodo
            self.__alto = alto
        else:
            print(f'Valor erroneo alto: {alto}')

    def __validar_valor(self, valor):
        return True if 0 < valor < 10 else False

    def __str__(self):
        """ el metodo str nos permite devolver un texto o valores cuando
        se llama el objeto, en ves de devolver el espacio en memoria
        es igual a devolver el estado actual de este objeto."""
        return f'Figura Geometrica: [Alto: {self.alto}, Ancho: {self.ancho}]' #dato, valor o datos a retornar cuando llaman al objeto.
