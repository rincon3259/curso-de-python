from Cuadrado import Cuadrado
from Rectangulo import Rectangulo

print('Creación objeto cuadrado'.center(50,'*'))
cuadrado1 = Cuadrado(-5, 'rojo')#pide 2 atributos, lado y color.
print(f'El color de la figura geometrica es = {cuadrado1.color}')
print(f'El ancho de mi figura es = {cuadrado1.ancho}')
print(f'El alto de mi figura es = {cuadrado1.alto}')
print(f'El area del cuadrado es = {cuadrado1.calcular_area()}')
#print(Cuadrado.mro())
#[<class 'Cuadrado.Cuadrado'>, <class 'FiguraGeometrica.FiguraGeometrica'>, <class 'Color.Color'>, <class 'object'>]
print(cuadrado1)

print('Creación objeto rectangulo'.center(50,'*'))
rectangulo1 = Rectangulo(5, -3, 'azul')#pide 3 atributos, base, altura y color.
print(f'El color de la figura geometrica es = {rectangulo1.color}')
print(f'El ancho de mi figura es = {rectangulo1.ancho}')
print(f'El alto de mi figura es = {rectangulo1.alto}')
print(f'El area del Rectangulo es = {rectangulo1.calcular_area()}')
#print(Rectangulo.mro())
print(rectangulo1)

print('Creación objeto rectangulo'.center(50,'*'))
rectangulo2 = Rectangulo(6, 3, 'verde')#pide 3 atributos, base, altura y color.
print(f'El color de la figura geometrica es = {rectangulo2.color}')
print(f'El ancho de mi figura es = {rectangulo2.ancho}')
print(f'El alto de mi figura es = {rectangulo2.alto}')
print(f'El area del Rectangulo es = {rectangulo2.calcular_area()}')
#print(Rectangulo.mro())
rectangulo2.ancho = -2
rectangulo2.alto = -6
rectangulo2.alto = 9
print(rectangulo2)