"""
Definir una clase padre llamada Vehiculo y dos clases hijas llamadas Coche y Bicicleta,
las cuales heredan de la clase Padre Vehiculo.
La clase padre debe tener los siguientes atributos y métodos

Vehiculo (Clase Padre):
-Atributos (color, ruedas)
-Métodos ( __init__() y __str__ )

Coche  (Clase Hija de Vehículo) (Además de los atributos y métodos heradados de Vehículo):
-Atributos ( velocidad (km/hr) )
-Métodos ( __init__() y __str__() )

Bicicleta  (Clase Hija de Vehículo) (Además de los atributos y métodos heradados de Vehículo):
-Atributos ( tipo (urbana/montaña/etc )
-Métodos ( __init__() y __str__() )
"""

class Vehiculo:#Clase padre
    def __init__(self, color, ruedas):
        self.color = color
        self.ruedas = ruedas

    def __str__(self):
        return f'Vehiculo: [Color: {self.color}, ruedas: {self.ruedas}]'

class Coche(Vehiculo):# Clase hija de Vehiculo
    def __init__(self, color, ruedas, velocidad):
        self.velocidad = velocidad
        super(). __init__(color, ruedas)

    def __str__(self):
        return f'Coche: [Velocidad: {self.velocidad} (Km/hr)], {super().__str__()}'

class Bicibleta(Vehiculo):# Clase hija de Vehiculo
    def __init__(self, color, ruedas, tipo):
        self.tipo = tipo
        super(). __init__(color, ruedas)

    def __str__(self):
        return f'Bicicleta: [tipo: {self.tipo}], {super().__str__()}'


veiculo1 = Vehiculo('azul', 2)
coche1 = Coche('negro', 4, 150)
bicibleta1 = Bicibleta('azul con blanco', 2, 'montaña')

print(veiculo1)
#Vehiculo: [Color: azul, ruedas: 2]
print(coche1)
#Coche: [Velocidad: 150 (Km/hr)], Vehiculo: [Color: negro, ruedas: 4]
print(bicibleta1)
#Bicicleta: [tipo: montaña], Vehiculo: [Color: azul con blanco, ruedas: 2]