from Persona import * #Importar todas las clases del modulo Persona.
""" Podemos crear clases de tipo persona y de tipo empleado """

persona1 = Persona('Juan', 28)
print(persona1) #Ejecuta de manera indirecta el metodo str()
#print(persona1.__str__()) No es necesario utilizar esta sintaxis ya que print() realiza la llamada de manera automatica.

Empleado1 = Empleado('Karla', 30, 2700000)
print(Empleado1)