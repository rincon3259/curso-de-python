class Persona: #Clase padre
    """ Defino la clase padre llamada Persona, como dato; todas las clases heredan
    de la clase object aunque no lo especipiquemos """
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
        pass
    def __str__(self):
        """ el metodo str nos permite debolver un texto o valores cuando
        se llama el objeto, en ves de devolver el espacio en memoria
        es igual a devolver el estado actual de este objeto."""
        return f'persona: [Nombre: {self.nombre}, Edad: {self.edad}]' #dato, valor o datos a retornar cuando llaman al objeto.
    pass

class Empleado(Persona): #clase hija
    """ Defino la clase hija y hereda de la clase Persona """
    def __init__(self, nombre, edad, sueldo):
        super(). __init__(nombre, edad) #Con el metodo super() podemos acceder a los atributos de la clase padre
        self.sueldo = sueldo
        pass
    def __str__(self):
        return f'Empleado: [Sueldo: {self.sueldo}], {super().__str__()}' #Estamos sobre escribiendo el metodo str de la clase padre para concatenar sueldo.
    pass

# empleado1 = Empleado('Juan', 28, 2700000)
# print(empleado1.nombre)
# print(empleado1.edad)
# print(empleado1.sueldo)
