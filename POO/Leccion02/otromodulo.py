# otromodulo.py
import mimodulo #Forma de importar #1 un modulo
print(mimodulo.suma(4, 3))   # 7
print(mimodulo.resta(10, 9)) # 1

from mimodulo import suma, resta #Forma de importar #2 un modulo con funciones especificas o limitadas
print(suma(4, 3))   # 7
print(resta(10, 9)) # 1

from mimodulo import * #Forma de importar #3 un modulo con todas sus funciones.
print(suma(4, 3))   # 7
print(resta(10, 9)) # 1