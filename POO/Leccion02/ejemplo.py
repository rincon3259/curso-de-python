from carpeta.modulo import *
hola()
# Hola

import sys
print(sys.path)

import moduloconnombrelargo #Forma de importar #1
print(moduloconnombrelargo.hola)

import moduloconnombrelargo as m #Forma de importar #2
print(m.hola)

print(dir())
# ['__annotations__', '__builtins__', '__cached__', '__doc__',
# '__file__', '__loader__', '__name__', '__package__', '__spec__']

print(__file__)
#curso-de-python\POO\Leccion02\ejemplo.py

mi_variable = "Python"
def mi_funcion():
    pass

print(dir())
# ['__annotations__', '__builtins__', '__cached__', '__doc__',
# '__file__', '__loader__', '__name__', '__package__', '__spec__',
# 'hola', 'm', 'mi_funcion', 'mi_variable', 'moduloconnombrelargo']
