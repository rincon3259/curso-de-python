# ejemplo #1 ******************
class Clase1:
    pass
class Clase2:
    pass
class Clase3(Clase1, Clase2): #Hereda métodos y atributos de 2 clases.
    pass

print(Clase3.__mro__)
# (<class '__main__.Clase3'>, <class '__main__.Clase1'>, <class '__main__.Clase2'>, <class 'object'>)

# ejemplo #2 ***************
class Clase1:
    pass
class Clase2(Clase1): #Esta clase hereda de una clase padre
    pass
class Clase3(Clase2): #Esta clase hereda de una clase hija que hereda de una clase padre
    pass

print(Clase3.__mro__)

# ejemplo #3 ***************
class Clase1:
    pass
class Clase2:
    pass
class Clase3:
    pass
class Clase4(Clase1, Clase3, Clase2): #el MRO depende del orden en el que las clases son pasadas: 1, 3, 2.
    pass
print(Clase4.__mro__)
# (<class '__main__.Clase4'>, <class '__main__.Clase1'>, <class '__main__.Clase3'>, <class '__main__.Clase2'>, <class 'object'>)