class Aritmetica:
    """
    Clase Aritmetica para realizar las operaciones de suma, resta, etc.
    """
    def __init__(self, operandoA, operandoB):
        self.operandoA = operandoA
        self.operandoB = operandoB

    def sumar(self):
        return self.operandoA + self.operandoB

    def restar(self):
        return self.operandoA - self.operandoB

    def multiplicar(self):
        return self.operandoA * self.operandoB

    def dividir(self):
        return self.operandoA / self.operandoB

aritmetica1 = Aritmetica(5,3)
print(f'sumar: {aritmetica1.sumar()}')
print(f'restar: {aritmetica1.restar()}')
print(f'multiplicar: {aritmetica1.multiplicar()}')
print(f'dividir: {aritmetica1.dividir():.2f}') #con :.2f limitamos los decimales a 2 digitos