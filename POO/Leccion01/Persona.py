# teoria clases es nombre del conjunto o esquema objeto es el articulo del conjunto o la particularidad nueva.
class Persona :
    def __init__ (self , nombre , apellido , edad) :
        self.__nombre = nombre #atributo encapsulado por doble guíon.
        self.__apellido = apellido #El atributo no se fuede usar fuera de la clase.
        self.__edad = edad #atributo encapsulado por doble guíon.

    @property #decorador para utilizar el metodo como atributo o variable fuera de la clase
    def nombre (self) : #metodo get para recuperar el atributo nombre fuera de la clase.
        print('Llamando método get nombre')
        return self.__nombre

    @nombre.setter #decorador para editar como atributo o variable fuera de la clase al atributo
    def nombre (self, nombre) :#metodo set para editar el atributo nombre fuera de la clase.
        print('Llamando método set nombre')
        self.__nombre = nombre

    @property
    def apellido (self):
        return self.__apellido

    @apellido.setter
    def apellido(self, apellido):
        self.__apellido = apellido

    @property
    def edad (self):
        return self.__edad

    @edad.setter
    def edad(self, edad):
        self.__edad = edad

    def mostrar_detalle(self):
        print(f'Persona {self.__nombre}, {self.__apellido}, {self.__edad}')

    def __del__(self):
        print(f'Persona {self.__nombre} {self.__apellido}')

if __name__ == '__main__': # Valida si se esta ejecutando este modulo para correr el codigo de no ser asi no ejecuta este bloque de codigo.
    print(type(Persona))
    persona1 = Persona('Juan', 'Perez', 28) #Se envia a la clase 3 variables que seran utilizadas por el metodo init o por el constructor de la clase.
    print(f'Objeto persona 1: {persona1.nombre}, {persona1.apellido}, {persona1.edad}')
    print(type(persona1)) #persona1 hace uso de la clase Persona para contruir un objeto o individuo,
    persona2 = Persona('Karla', 'Gomez', 30)
    print(f'Objeto persona 2: {persona2.nombre}, {persona2.apellido}, {persona2.edad}')

    persona1.nombre = 'Juan Carlos' #modificar atributos de un objeto.
    persona1.apellido = 'Juarez'#modificar atributos de un objeto.
    persona1.edad = 25 #modificar atributos de un objeto.
    print(f'Objeto persona 1: {persona1.nombre}, {persona1.apellido}, {persona1.edad}')
    print('***** UTILIZANDO METODO mostar_detalle: *******')
    persona1.mostrar_detalle()
    persona2.mostrar_detalle()
    # objeto : Juan Carlos Juarez 25
    # objeto : Karla Gomez 30
    print('***** otra manera de UTILIZANDO METODO mostar_detalle: *******')
    Persona.mostrar_detalle(persona1)
    Persona.mostrar_detalle(persona2)
    # objeto : Juan Carlos Juarez 25
    # objeto : Karla Gomez 30
    print('***** CREAR NUEVO ATRIBUTO: *******')
    persona1.telefono = 30215487897
    print(persona1.telefono)
    print('***** CREAR NUEVO ATRIBUTO DE UN OBJETO TUPLA Y DICCIONARIO: *******')
    persona1 = Persona('Juan', 'Perez', 28, '30215487897', 2, 3, 5, m='manzana', p='pera'  ) #Despues de definir, nombre apellido y edad definimos valores para la tupla y luego valores clave valor para el diccionario
    persona1.mostrar_detalle()

    print(__name__)