class Cubo:
    """
    Crear una clase de cubo, con 3 atributos; ancho, alto y profundidad
    Esta clase tiene un metodo para calcular el volumen del cubo la formula es
    volumen = ancho * alto * profundidad.
    Los datos deben ser ingresados por el usuario.
    """
    def __init__(self, a, h, p):
        self.ancho = a
        self.alto = h
        self.profundidad = p

    def cacular_volumen(self):
        return self.ancho * self.alto * self.profundidad

a = int(input('Proporciona el ancho del cubo: '))
h = int(input('Proporciona la altura del cubo: '))
p = int(input('Proporciona la altura del cubo: '))
cubo1 = Cubo(a,h,p)
print(f'El volumen del cubo es: {cubo1.cacular_volumen()}')