from Persona import Persona

# La comprovación __name__ es opcional para saber si se esta ejecutando del modulo acyal o se esta llamando desde otro modulo.
if __name__ == '__main__': # Valida si se esta ejecutando este modulo para correr el codigo de no ser asi no ejecuta este bloque de codigo.
    print('Creación de objetos'.center(50, '-'))
    persona1 = Persona('Karla', 'Gomez', 30)
    persona1.mostrar_detalle()
    print(__name__)
    print('Eliminación objetos'.center(50, '-'))
    del persona1