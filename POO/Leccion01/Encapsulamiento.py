class Persona: #Definición de la clase Persona, encapsulamiento de datos.

    def __init__(self, nombre, apellido, edad):
        self._nombre = nombre # Con el guión bajo estamos indicando que no deberian acceder a esta variable fuera de esta clase
        self.__apellido = apellido
        self.edad = edad

    def mostrar_detalle(self):
        print(f'objeto : {self._nombre} {self.__apellido} {self.edad}')

persona1 = Persona('Juan', 'Perez', 28) #Se envia a la clase 3 variables que seran utilizadas por el metodo init o por el constructor de la clase.
persona1._nombre = 'Juan Carlos' #De esta manera no deberiamos editar atributos indicados con guión bajo.
persona1.__apellido = 'Perez Martines' #Con dogle guión bajo restringimos la idición del atributo.
persona1.mostrar_detalle()
#Objeto persona 1: Juan Carlos, Perez, 28
# *************************************************************************

class Clase:
    atributo_clase = "Hola"   # Accesible desde el exterior
    __atributo_clase = "Hola" # No accesible

    # No accesible desde el exterior
    def __mi_metodo(self):
        print("Haz algo")
        self.__variable = 0

    def metodo_normal(self):# Accesible desde el exterior
        # El método si es accesible desde el interior
        self.__mi_metodo()

    @property #decorador, modifica el funcionamiento de una función
    def atributo_clase(self): #Se crea como metodo get de recuperación del atributo __atributo_clase
        return self.__atributo_clase #retonar el atributo __atributo_clase que esta encapsulado con doble guión bajo

    @atributo_clase.setter#decorador tipo set para modificar el metodo atributo_clase
    def atributo_clase(self, atributo_clase): #Se crea como metodo set para editar el atributo __atributo_clase
        self.__atributo_clase = atributo_clase #modifica el atributo __atributo_clase dentro de la clase.


mi_clase = Clase()
# print(mi_clase.atributo_clase()) # Error por utilizar el decorador property que usa un metodo como si fuera un atributo
print(mi_clase.atributo_clase) #accede al atributo de clase por metodo get como si fuera atributo
mi_clase.atributo_clase = 'hola mundo metodo set y get'
print(mi_clase.atributo_clase) #accede al atributo de clase por metodo get como si fuera atributo

# Hola
# hola mundo metodo set y get


#mi_clase.__atributo_clase  # Error! El atributo no es accesible
#mi_clase.__mi_metodo()     # Error! El método no es accesible
mi_clase.atributo_clase     # Ok!
mi_clase.metodo_normal()    # Ok!
mi_clase._Clase__atributo_clase
# 'Hola'
mi_clase._Clase__mi_metodo()
# 'Haz algo'

print(dir(mi_clase))
#['_Clase__atributo_clase', '_Clase__mi_metodo', '_Clase__variable',
#'__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__',
#'__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__',
#'__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__',
#'__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__',
#'__str__', '__subclasshook__', '__weakref__', 'atributo_clase', 'metodo_normal']

print(dir())
# ['__annotations__', '__builtins__', '__cached__', '__doc__',
# '__file__', '__loader__', '__name__', '__package__', '__spec__']

def mi_decorador(funcion):
    def nueva_funcion(a, b):
        print("Se va a llamar")
        c = funcion(a, b)
        print("Se ha llamado")
        return c
    return nueva_funcion

@mi_decorador
def suma(a, b):
    print("Entra en funcion suma")
    return a + b

print(suma(5,8))

# Se va a llamar
# Entra en funcion suma
# Se ha llamado
#13

@mi_decorador
def resta(a ,b):
    print("Entra en funcion resta")
    return a - b

print(resta(5, 3))

# Se va a llamar
# Entra en funcion resta
# Se ha llamado
# 2