class Rectangulo:
    """
    La clase Rectangulo tiene 2 atributos Base y Altura,
    esta clase tiene 1 metodo calcular area, formula del area.
    A = B x H
    Los valores de base y altura los debe proporcionar el usuario.
    """
    def __init__(self, B, H):
        self.Base = B
        self.Altura = H

    def cacular_area(self):
        return self.Base * self.Altura

B = int(input('Proporciona la base del Rectangulo: '))
H = int(input('Proporciona la altura del Rectangulo: '))
rectangulo1 = Rectangulo(B, H)
print(f'El área del Rectangulo es: {rectangulo1.cacular_area()}')
