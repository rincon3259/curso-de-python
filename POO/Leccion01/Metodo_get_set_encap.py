class Persona :
    def __init__ (self , nombre , apellido , edad) :
        self.__nombre = nombre #atributo encapsulado por doble guíon.
        self.__apellido = apellido #El atributo no se fuede usar fuera de la clase.
        self.__edad = edad #atributo encapsulado por doble guíon.

    @property #decorador para utilizar el metodo como atributo o variable fuera de la clase
    def nombre (self) : #metodo get para recuperar el atributo nombre fuera de la clase.
        print('Llamando método get nombre')
        return self.__nombre

    @nombre.setter #decorador para editar como atributo o variable fuera de la clase al atributo
    def nombre (self, nombre) :#metodo set para editar el atributo nombre fuera de la clase.
        print('Llamando método set nombre')
        self.__nombre = nombre

    @property
    def apellido (self):
        return self.__apellido

    @apellido.setter
    def apellido(self, apellido):
        self.__apellido = apellido

    @property
    def edad (self):
        return self.__edad

    @edad.setter
    def edad(self, edad):
        self.__edad = edad

    def mostrar_detalle(self):
        print(f'Persona {self.__nombre}, {self.__apellido}, {self.__edad}')

# fin de la clase Persona

persona1 = Persona('Juan', 'Perez', 28)
persona1.mostrar_detalle()
persona1.nombre = 'Juan Carlos'
persona1.mostrar_detalle()
persona1.apellido = 'Perez Martinez'
persona1.mostrar_detalle()
persona1.edad = 30
persona1.mostrar_detalle()
