# Set en python
planetas ={'Marte', 'Jupiter', 'Venus'} # Definir un set
print(planetas)
print(len(planetas)) # Conocer el tamaño de un set
print('Jupiter' in planetas) #Revisar si un elemento está presente
planetas.add('Tierra') # Agregar un elemento a un set
print(planetas)
planetas.add('Tierra') # Set no permite duplicar elementos
print(planetas) #{'Marte', 'Venus', 'Tierra', 'Jupiter'}
planetas.remove('Tierra') # Elimina un elemento de un set si no aparece el elemnto debuelve el error KeyError.
print(planetas)
planetas.discard('Tierra') # Elimina un elemento de un set si no aparece el elemento no hace nada.
print(planetas)
planetas.clear() # elimina todos los elementos de un set.
print(planetas)
del planetas #Elimina la variable.