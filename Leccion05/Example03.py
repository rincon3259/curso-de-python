# Definir una tupla
frutas = ('Naranja', 'Platano', 'Guayaba')
# Saber el largo de una tupla
print(len(frutas))
# imprimir tupla
print(frutas)
# Acceder a valores por el indice
print(frutas[0])
print(frutas[-1])
# Acceder a valores por rangos
print(frutas[0:2]) #Sin incluir el ultimo indice
# Recorrer los elementos de una tupla
print('************ Imprime los elementos de una tupla uno a uno. ******************')
for fruta in frutas:
    print(fruta) # Imprime con salto de linea

for fruta in frutas:
    print(fruta, end=' ') # Imprime sin salto de linea
# convertir una tupla en lista para cambiar elementos dentro de la tupla
frutasLista = list(frutas)#convierte a lista
frutasLista[0] = 'Pera' #Modifica el valor de indice 0.
frutas = tuple(frutasLista) #Convierte a Tupla
print('\n', frutasLista)
#  Eliminar la tupla
del frutas

