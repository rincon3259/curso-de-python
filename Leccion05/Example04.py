# Ejercicio practico %% convertir una tupla en lista que solo incluya los calores menores a 5
tupla = (13, 1, 8, 3, 2, 5, 8)
lista = []
for i in tupla:
    if i < 5:
        lista.append(i)

print(lista) #[1, 3, 2]

