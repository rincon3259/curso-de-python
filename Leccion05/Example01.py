# Listas
nombres = ['Juan', 'karla', 'Ricardo', 'Maria']
#Imprimir la lista de nombres
print(nombres)
# Acceder a los elementos de una lista
print(nombres[0])
print(nombres[1])
# Acceder a los elementos de una lista de manera inversa
print(nombres[-1])
print(nombres[-2])
print('********************************')
#Imprimir un rango de una lista
print(nombres[0:2]) # sin incluir el indice 2
print(nombres[:3]) # desde el inicio sin incluir el indice 3
print(nombres[1:]) # Desde un valor hasta el ultimo
print('********************************')
# Cambiar un valor dentro de una lista
nombres[3] = 'Ivone'
print(nombres)
print('********************************')
# literar una lista
for nombre in nombres:
    print(nombre)
else:
    print('No existen más nombres en la lista')
print('********************************')
# Saber el largo de una lista.
print(len(nombres))
print('********************************')
# Agregar un elemento a una list
nombres.append('Lorenzo')
print(nombres)
print('********************************')
# Insertar un elemento en un indice en especifico
nombres.insert(1,'Octavio')
print(nombres)
print('********************************')
# Remover un elemento de una lista
nombres.remove('Octavio')
print(nombres)
print('********************************')
# Eliminar un elemento de una lista segun su indice
nombres.remove(nombres[1])
print(nombres)
print('********************************')
# Eliminar el ultimo elemento de una lista
nombres.pop()
print(nombres)
print('********************************')
# Eliminar un elemento de una lista segun su indice
del nombres[0]
print(nombres)
print('********************************')
# limpiar o vaciar una lista
nombres.clear()
print(nombres)
# []
print('********************************')
# Eliminar una lista por completo
del nombres
print(nombres)
print('********************************')


