# Ejercicio practico *** para practicar el concepto de rango
# 1.) Iterar un rango de 0 a 10 e imprimir números divisibles entre 3.
print('************* nuemeros de 1 a 10 divisibles en 3 *****************')
rango = range(11)
for i in rango:
    if i % 3 == 0:
        if i == 0:
            continue
        else:
            print(f'{i} Es divisible en 3')

#  2.) crecar un rango de 2 a 6, e imprimirlos.
print('************rango de 2 a 6 ******************')
rango = range(2, 7)
for i in rango:
    print(i)
#  3.) crecar un rango de 3 a 10, pero con incremento de 2 en 2.
print('****************rango de 3 a 10 con incremento de 2 en 2 **************')
rango = range(3, 11, 2)
for i in rango:
    print(i)