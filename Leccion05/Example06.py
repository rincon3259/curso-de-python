# Dicionarios (jey, value)
diccionario = {
    'IDE':'Integrated Development Environment',
    'OOP':'Object Oriented Programming',
    'DBMS':'Database Management System',
} #Definimos un diccionario
print(diccionario)
print(len(diccionario)) #Conocer el largo de un diccionario
print(diccionario['IDE']) #acceder a un elemento (key) y si no existe devuelve error KeyError
print(diccionario.get('OOP')) #acceder a un con el metodo get (key) si no existe devuelve None
diccionario['IDE'] = 'integrated development environment' #Modoficar elementos
print(diccionario)
# iterar diccionario devuelve la key
for k in diccionario:
    print(k)
print('********************************')
# iterar diccionario devuelve la key y el value
for k, v in diccionario.items():
    print(k, v)
print('********************************')
# iterar diccionario devuelve la key
for k in diccionario.keys():
    print(k)
print('********************************')
# iterar diccionario devuelve el valor
for v in diccionario.values():
    print(v)
print('********************************')
# Comprobar si existe una llave
print('IDE' in diccionario)
print('ide' in diccionario) #Se deben respetar mayusculas y minusculas o devuelve False
print('********************************')
# Agregar un elemento al diccionario
print(diccionario)
diccionario['PK'] = 'Primary Key'
print(diccionario)
print('********************************')
# Remover un elemento del diccionario
diccionario.pop('DBMS')
print(diccionario)
print('********************************')
# limpiar el diccionario
diccionario.clear()
print(diccionario)
print('********************************')
# eliminar el diccionario
del diccionario
print(diccionario)
