#Ejercicio practico %%%% se le pede al usuario ingresar un valor numerico entre 0 y 5
# y vamos a validar si el valor ingresado si se encuentra en dicho rango  %%%%
numero = int(input('Digita un numero entre 0 y 5: '))
if (numero >= 0) and (numero <= 5):
    print(f'has digitado el numero {numero} y si esta en el rango de 0 a 5')
else:
    print(f'has digitado el numero {numero} y no esta en el rango de 0 a 5')

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
