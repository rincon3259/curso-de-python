#Operadores Matematicos.
operandoA = 5
operandoB = 2
suma = operandoA + operandoB #Suma de variables.
print('Resultado de la suma: ',suma) #forma simple de imprimir en pantalla.
print(f'Resultado de la suma: {suma}') #Imprimir utilizando el literal format.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
resta = operandoA - operandoB #resta de variables.
print(f'Resultado de la resta: {resta}') #Imprimir utilizando el literal format.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
multiplicacion = operandoA * operandoB #Multiplicación de variables.
print(f'Resultado de la Multiplicación: {multiplicacion}') #Imprimir utilizando el literal format.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
division = operandoA / operandoB #Division de variables y devuelve en tipo flotante.
print(f'Resultado de la Division: {division}') #Imprimir utilizando el literal format.
division = operandoA // operandoB #division de variables y devuelve entero.
print(f'Resultado de la Division: {division}') #Imprimir utilizando el literal format.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
modulo = operandoA % operandoB #resto de la division (modulo) de variables.
print(f'Resultado resto de la division (modulo): {modulo}') #Imprimir utilizando el literal format.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
exponente = operandoA ** operandoB #exponente de variables.
print(f'Resultado exponente: {exponente}') #Imprimir utilizando el literal format.