#Operadores de Comparación.
a = 4
b = 2

Resultado = a == b #Igual que
print(f'Resultado de comparar a con b == : {Resultado}')

Resultado = a != b #diferente a
print(f'Resultado de comparar a con b != : {Resultado}')

Resultado = a > b #Mayor que
print(f'Resultado de comparar a con b > : {Resultado}')

Resultado = a < b #Menor que
print(f'Resultado de comparar a con b < : {Resultado}')

Resultado = a >= b #Mayor o igual que
print(f'Resultado de comparar a con b >= : {Resultado}')

Resultado = a <= b #Menor o igual que
print(f'Resultado de comparar a con b <= : {Resultado}')
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
