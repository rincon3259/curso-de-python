#Operadores de asignación.
miVariable = 10 #operador de asignación
print(miVariable)
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
miVariable += 1 #reasignación
print(miVariable)
miVariable = miVariable + 1 #reasignación
print(miVariable)
miVariable -= 2 #reasignación
print(miVariable)
miVariable *= 3 #reasignación
print(miVariable)
miVariable //= 2 #reasignación
print(miVariable)
miVariable /= 2 #reasignación
print(miVariable)