#Sintaxis if else simplificada (Operador Ternario)
condicion = False

if condicion:
    print('Condición verdadera')
else:
    print('Condición Falsa')

print('Condición verdadera') if condicion else print('Condición Falsa') #Operador Ternario del if
