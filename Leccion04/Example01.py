# Ciclo while/ mientras que sea verdadero ejecute...

contador = 0
while contador < 3:
    print(contador)
    contador += 1
else:
    print('Fin del ciclo while')

# 0
# 1
# 2
# Fin del ciclo while