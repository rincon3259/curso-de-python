# Ejercicio practico palabra break dentro de los ciclos/
for letra in 'Holanda':
    if letra == 'a':
        print(f'Letra encontrada: {letra}')
        break
else:
    print('Fin del ciclo for')

# Letra encontrada: a
# Letra encontrada: a
# Fin del ciclo for