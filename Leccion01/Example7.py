x= 2.1 # numeros desimales o float
print(x)
print(type(x))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

x= 10 # numeros enteros
print(x)
print(type(x))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

x= 21 + 78j # numeros complejos
print(x)
print(type(x))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

x= "Hola mundo" # VAriable de tipo string o cadena de caracteres
y= 'Hola mundo' # Tipo string soporta comillas dobles y sensillas
print(x)
print(type(x))
print(y)
print(type(y))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

lista = ['abcd', 786, 2.23, 'john', 70.2] #listas
tinylist = [123, 'john']

print (lista) # Imprime lista completa
print (lista [0]) # Imprime el primer elemento de la lista
print (lista [1: 3]) # Imprime elementos desde el 2 hasta el 3
print (lista [2:]) # Imprime elementos a partir del 3er elemento
print (tinylist * 2) # Imprime la lista dos veces
print (lista + tinylist) # Imprime listas concatenadas

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

tupla = ('abcd', 786, 2.23, 'john', 70.2) #Tuplas
tinytuple = (123, 'john')

print (tupla) # Imprime lista completa
print (tupla [0]) # Imprime el primer elemento de la lista
print (tupla [1: 3]) # Imprime elementos desde el 2 hasta el 3
print (tupla [2:]) # Imprime elementos a partir del 3er elemento
print (tinytuple * 2) # Imprime la lista dos veces
print (tupla + tinytuple) # Imprime listas concatenadas

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

dict = {} #Diccionario
dict ['uno'] = "Este es uno" #Clave y valor
dict [2] = "Esto es dos" #Clave y valor

tinydict = {'name': 'john', 'code': 6734, 'dept': 'sales'} #Diccionario Clave luego de los dos puntos el valor


print(dict ['uno']) # Imprime el valor de la tecla 'uno'
print(dict [2]) # Imprime el valor de la tecla 2
print(tinydict) # Imprime el diccionario completo
print(tinydict.keys()) # Imprime todas las claves
print(tinydict.values()) # Imprime todos los valores

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

x= True# Variable de tipo Boolean
y= False # Variable de tipo Boolean
print(x)
print(type(x))
print(y)
print(type(y))