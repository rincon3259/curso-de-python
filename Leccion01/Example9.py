#confundir la concatenación con sumas
numero1 = "1"
numero2 = "2"
print(numero1+numero2)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
numero1 = 1
numero2 = 2
print(numero1+numero2)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
numero1 = "1"
numero2 = "2"
print("Concatenación:", numero1 + numero2)
print("Suma: ", int(numero1) + int(numero2))