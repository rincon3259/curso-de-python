from msilib.schema import Binary
#concatenación de cadenas
MiGrupoFavorito= "Aerosmith"
print("Mi grupo favorito es:"+MiGrupoFavorito)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
x = 'Aerosmith '
y = 5000
print(x + str(y))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
x = 'Aerosmith '
y = 5000
print("%s%s" % (x, y))

#Utilizando la función format() ++++++++++++++++++++++++++++++++++++++++++++++++
y = 123
print(format(y, "d"))
print(type(format(y, "d")))
print(type(y))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
y = 123.456789
print(format(y, "f"))
print(type(format(y, "f")))
print(type(y))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
y : Binary = 10
print(format(y, "b"))
print(type(format(y, "b")))
print(type(y))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
y = 1234
print(format(y, "*>+7,d"))
print(type(format(y, "*>+7,d")))
print(type(y))

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
y = 123.4567
print(format(y, "^-09.3f"))
print(type(format(y, "^-09.3f")))
print(type(y))

#Utilizando la función format() para concatenar cadenas+++++++++++++++++++++++
x = 'Aerosmith '
y = 5000
print("{}{}".format(x, y))

#Utilizando  f-string para concatenar cadenas+++++++++++++++++++++++++++++++++
x = 'Aerosmith '
y = 5000
print(f'{x}{y}')

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MiGrupoFavorito="Metallica"
comentario = "The Best Rock Band"
print("Mi grupo favorito es: "+MiGrupoFavorito+" "+comentario)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MiGrupoFavorito="Metallica"
comentario = "The Best Rock Band"
print("Mi grupo favorito es:", MiGrupoFavorito, comentario)